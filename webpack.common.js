const _ = require('underscore'),
    path = require('path'),
    webpack = require('webpack'),
    CleanWebpackPlugin = require('clean-webpack-plugin'),
    CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin'),
    ExtractTextWebpackPlugin = require('extract-text-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),

    ROOT = __dirname,
    RESOURCES_PATH = path.join(ROOT, 'resources'),
    VIEWS_PATH = path.join(RESOURCES_PATH, 'views'),
    DIST_PATH = '/react/dist',

    VENDOR_ENTRIES = _.reduce([
        'bootstrap',
        'jquery',
        'popper.js',
        'react',
        'react-dom',
        'react-redux',
        'react-router',
        'react-router-redux',
        'redux-thunk'
    ], (memo, vendor, idx) => {
        memo['vendor-' + idx] = vendor;
        return memo;
    }, {}),
    VENDOR_ENTRY_KEYS = _.keys(VENDOR_ENTRIES),

    REACT_ENTRIES = {
        commonBundleScss: RESOURCES_PATH + '/sass/app.scss',
        appBundle: RESOURCES_PATH + '/react/appBundle.js'
    },

    PACKAGE_JSON = require('./package'),
    DOTENV = require('dotenv').config({
        path: process.cwd() + '/.env'
    }).parsed,
    ROOT_URI = DOTENV.APP_ROOT_URI;

module.exports = (opts={}) => {
    return (env='dev') => {
        let outputPath = path.join(ROOT, 'public', DIST_PATH);

        return {
            entry: _.extend(VENDOR_ENTRIES, REACT_ENTRIES),
            output: {
                path: outputPath,
                publicPath: ROOT_URI + DIST_PATH + '/',
                filename: '[name].[chunkhash].js'
            },
            plugins: [
                new webpack.DefinePlugin({
                    'process.env.NODE_ENV': JSON.stringify(env),
                    'process.env.ROOT_URL': JSON.stringify(ROOT_URI)
                }),
                new CleanWebpackPlugin([outputPath], {
                    root: ROOT
                }),
                new ExtractTextWebpackPlugin({filename: 'common-bundle.[contenthash].css'}),
                new webpack.ProvidePlugin({
                    $: 'jquery',
                    _: 'underscore',
                    React: 'react',
                    ReactDOM: 'react-dom'
                }),
                new CommonsChunkPlugin({
                    names: VENDOR_ENTRY_KEYS,
                    minChunks: Infinity
                }),
                new HtmlWebpackPlugin(createHtmlWebpackPluginOption('react/includes', 'appBundle', {
                    chunks: concatVendors(['appBundle', 'commonBundleScss'])
                }))
            ].concat(opts.plugins || []),
            devtool: opts.devtool || undefined,
            module: {
                rules: [{
                    test: /\.js$/,
                    loader: 'babel-loader',
                    query: {
                        'presets': ['es2015', 'react', 'stage-1']
                    },
                    include: path.join(__dirname, 'resources/react')
                }, {
                    test: /\.(s*)css$/,
                    use: ExtractTextWebpackPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            'css-loader',
                            {
                                loader: 'sass-loader',
                                options: {
                                    data: '$env:' + env + ';$imagePathBase:"' + ROOT_URI + '";'
                                }
                            }
                        ]
                    })
                }]
            }
        };
    };
};

function concatVendors(array) {
    return VENDOR_ENTRY_KEYS.concat(array);
}

function createHtmlWebpackPluginOption(filePath, fileName, opts={}) {
    opts.filename = createPHPViewsPath(filePath, fileName);
    opts.template = createPHPViewsPath(filePath, fileName, true);
    opts.chunksSortMode = 'dependency';
    opts.version = PACKAGE_JSON.version;
    return opts;
}

function createPHPViewsPath(filePath, fileName, isTemplate) {
    return VIEWS_PATH +  '/' + filePath + (isTemplate ? '/templates' : '') + '/' + fileName + '.blade.php';
}
