import React, {PureComponent} from 'react';

export default class AsyncComponent extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            Component: null,
            conponentProps: null
        };
    }

    componentWillMount() {
        let props = this.props;

        if (!this.state.Component) {
            props.moduleProvider()
                .then(Component => this.setState({
                    Component: Component.default,
                    conponentProps: props.conponentProps
                }));
        }
    }

    render() {
        const {Component, conponentProps} = this.state;
        if (!Component) {
            return <div></div>;
        }
        return <Component {...conponentProps}/>;
    }
}
