import React from 'react';
import {Route} from 'react-router';

import CommonBundle from './commonBundle';
import {ROOT_URL} from './constants';
import App from './components/App';
import * as reducers from './reducers';

const Home = () => import('./components/Home');

class AppBundle extends CommonBundle {
    constructor() {
        super();
        super._reactDomRendor(reducers, this._createRoute.bind(this));
    }

    _createRoute() {
        let createAsyncComponent = super._createAsyncComponent;
        return <Route path={ROOT_URL} component={App}>
            <Route path="home" component={props => createAsyncComponent(props, Home)} />
        </Route>;
    }
}

new AppBundle();
