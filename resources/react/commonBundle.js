/* global ReactDOM */

import {browserHistory, Router} from 'react-router';
import {Provider} from 'react-redux';
import {routerReducer, syncHistoryWithStore} from 'react-router-redux';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import AsyncComponent from './components/AsyncComponent';

export default class CommonBundle {
    _reactDomRendor(reducers, createRoute) {
        let store = createStore(combineReducers({
            ...reducers,
            routing: routerReducer
        }), applyMiddleware(thunk));

        ReactDOM.render(
            <Provider store={store}>
                <div>
                    <Router history={syncHistoryWithStore(browserHistory, store)}>
                        {createRoute()}
                    </Router>
                </div>
            </Provider>,
            document.getElementById('root')
        );
    }

    _createAsyncComponent(props, moduleProvider) {
        return <AsyncComponent moduleProvider={moduleProvider} conponentProps={props}/>;
    }
}
