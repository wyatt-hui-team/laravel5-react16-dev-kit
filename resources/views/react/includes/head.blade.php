<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="">
<meta property="og:type" content="website" />
<meta property="og:url" content="{{URL::current()}}" />
<meta name="csrf-token" content="{{csrf_token()}}">

<link rel="icon" type="image/png" href="{{media_url('image/favicon-96x96.png')}}">

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    token = document.head.querySelector('meta[name="csrf-token"]');
    if (token) {
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }
</script>

<script src="<?= media_url("js/es5-sham.min.js") ?>"></script>
<script src="<?= media_url("js/es5-shim.min.js") ?>"></script>
<script src="<?= media_url("js/es6-sham.min.js") ?>"></script>
<script src="<?= media_url("js/es6-shim.min.js") ?>"></script>
<script src="<?= media_url("js/es7-shim@latest.js") ?>"></script>
<script src="<?= media_url("js/json3.min.js") ?>"></script>
