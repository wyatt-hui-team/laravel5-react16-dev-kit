<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <?php $pageTitle = 'Webapp';?>
        <title><?= $pageTitle ?></title>
        <meta property="og:title" content="<?= $pageTitle ?>" />
        <meta property="og:site_name" content="" />
        <meta property="og:description" content="" />
        @include('react.includes.head')
    </head>
    <body>
        <div>
            <div id="root"></div>
        </div>
        @include('react.includes.appBundle')
    </body>
</html>
