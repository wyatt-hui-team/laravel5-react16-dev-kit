<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (!defined('REACT_INDEX')) define('REACT_INDEX', 'react.index');

Route::group([], function ($router) {
    $router->redirect('/', 'home', 302);
    $router->view('/home', REACT_INDEX);
});
