const webpack = require('webpack');

module.exports = require('./webpack.common')({
    devtool: '#eval-source-map',
    plugins: [
        new webpack.NamedModulesPlugin()
    ]
});
