### Dependencies
* Composer (https://getcomposer.org/)
* Node.js (https://nodejs.org/)

### Installation
Installation for backend
* /
    ```
    composer install
    ```

* Copy `.env.example` to `.env`

Installation for frontend
* /
    ```
    npm install
    ```

### Development
Run webpack
* /
    ```
    npm run dev
    ```
    * (Optional) Watch changes
        ```
        npm run watch
        ```

### Deployment instructions
For version update, please edit the version number in
* package.json

### Building for source
For production:

* /.env
    * Define `APP_ENV` as `production`
* /
    ```
    npm run prod
    ```

### Team contacts
* Somebody (somebody@example.com)

### License
Somebody
