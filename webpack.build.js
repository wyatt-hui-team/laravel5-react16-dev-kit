const webpack = require('webpack');
    UnminifiedWebpackPlugin = require('unminified-webpack-plugin');

module.exports = require('./webpack.common')({
    plugins: [
        new webpack.HashedModuleIdsPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new UnminifiedWebpackPlugin()
    ]
});
